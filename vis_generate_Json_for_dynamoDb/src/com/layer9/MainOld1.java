package com.layer9;

import org.json.JSONArray;
import org.json.CDL;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;


public class MainOld1 {


    public static void main(String[] args) throws FileNotFoundException {

            String csvFile = "D:\\scan-store-received.csv";


            String line = "";
            // write your code here
            BufferedReader br = null;
            JSONArray dataList = new JSONArray();
            String result = "";
            try {
                int count = 0;
                String[] headerColumn = new String[30];//
                br = new BufferedReader(new FileReader(csvFile));
                line = br.readLine();
                while (line != null) {
                    Map dataDetails = new LinkedHashMap();
                    String[] line_split= line.split(",");
                    if (line_split.length>1) {
                        String timestamp = line_split[0];
                        timestamp = timestamp.replaceAll("\"","");
                        String xmlMessage = line_split[1].replaceAll("\"","");
                        if (xmlMessage != null && !xmlMessage.equals("") && xmlMessage.contains("postfill")
                                && timestamp.trim().compareTo("1583020800000") >= 0) {
                            //parse xml
                            int start = xmlMessage.indexOf("<tagvalue>");
                            int end = xmlMessage.indexOf("</tagvalue>");
                            String tagvalue = xmlMessage.substring(start + 10, end);
                     //       System.out.println("tagvalue " + tagvalue);
                            if (!tagvalue.equals("0")) {
                                dataDetails.put("tagvalue", tagvalue);
                                dataList.put(dataDetails);
                            }
                        }
                    }

                    line = br.readLine();
                }
                File fileFolder = new File("D:\\filterTagValue");
                    if (!fileFolder.exists()) {
                        if (fileFolder.mkdir()) {
                            System.out.println("Directory is created!");
                        } else {
                            System.out.println("Failed to create directory!");
                        }
                    }
                    // Write to file
                File file=new File("D:\\filterTagValue\\tagvalue.csv");
                String csv = CDL.toString(dataList);
                FileUtils.writeStringToFile(file, csv);


            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

}
