package com.layer9;

import java.io.*;
import java.sql.Timestamp;
import java.util.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;





public class Main_old {


    public static void main(String[] args) throws FileNotFoundException {

        String csvFile = "D:\\asset_part_17.csv";
        String line = "";
        String cvsSplitBy = ",";
        int countFile=0;
        int countFileTemp=0;
        int countCartella=0;
	// write your code here
        BufferedReader br = null;
        JSONArray dataList = new JSONArray();
        Map  dataTableFather = new LinkedHashMap();
        try {
            int count=0;
            String[] headerColumn = new String[30];//
            br = new BufferedReader(new FileReader(csvFile));
            line= br.readLine();
            while (line != null) {
                Map dataDetails = new LinkedHashMap();
                Map dataItem = new LinkedHashMap();
                Map dataPutRequest = new LinkedHashMap();

                // use comma as separator
                line= line.replace("\"","");
                String[] data =  line.split(cvsSplitBy);

                if (count ==0){
                    //header column
                    for(int i=0;i<data.length;i++){
                       /* if (data[i].trim().equals("id"))
                            headerColumn[i]="tagid";
                        else*/
                            headerColumn[i]=data[i].trim().toLowerCase();
                    }

                }else{
                    for(int i=0;i<data.length;i++){
                       // System.out.println(headerColumn[i]);
                        if (!data[i].equals("")) {
                            JSONObject dataType = new JSONObject();
                            data[i] = data[i].replace("�"," ").trim();
                            if (data[i].trim().equals("true") || data[i].trim().equals("false"))
                                dataType.put("BOOL",Boolean.valueOf(data[i].trim()));
                            else {
                                try{

                                   //int

                                    if (headerColumn[i].equals("rolename")){

                                        JSONArray rolesList = new JSONArray();
                                        JSONObject l = new JSONObject();
                                        String[] roles = data[i].trim().split(";");
                                        for(int a=0;a<roles.length;a++){
                                            JSONObject dataRoles = new JSONObject();
                                            dataRoles.put("S",roles[a].trim());
                                           /* JSONObject role = new JSONObject();
                                            role.put("role", dataRoles);*/
                                            rolesList.add(dataRoles);
                                        }
                                        l.put("L",rolesList);
                                        dataDetails.put("roles", l);
                                    }
                                    else {
                                        int value= Integer.valueOf(data[i].trim());
                                        if (headerColumn[i].equals("tagid") || headerColumn[i].equals("tagvalue"))
                                            dataType.put("S", data[i].trim());
                                        else
                                            dataType.put("N", data[i].trim());
                                    }
                                }catch(Exception e){
                                    //String
                                    dataType.put("S",data[i].trim());
                                }
                            }

                           // JSONObject objectSingle = new JSONObject();
                          //  objectSingle.put(headerColumn[i], dataType);
                            if (!headerColumn[i].equals("rolename"))
                                dataDetails.put(headerColumn[i], dataType);
                        }
                    }
                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    JSONObject ts = new JSONObject();
                    String tsString = String.valueOf(timestamp.getTime());
                    ts.put("S",tsString);
                    dataDetails.put("lastfill_event_timestamp",ts);
                    dataDetails.put("last_event_timestamp",ts);
                    dataItem.put("Item", dataDetails);
                    dataPutRequest.put("PutRequest", dataItem);
                    dataList.add(dataPutRequest);
                }
                count++;
                if (count==1){
                    //Write JSON file

                    dataTableFather.put("scan-store-admin-asset",dataList);

                   // System.out.println(JSONValue.toJSONString(dataTableFather));
                    File fileFolder = new File("D:\\asset_"+ countCartella);
                    if (!fileFolder.exists()) {
                        if (fileFolder.mkdir()) {
                            System.out.println("Directory is created!");
                        } else {
                            System.out.println("Failed to create directory!");
                        }
                    }
                    // Write to file
                    FileWriter file = new FileWriter("D:\\asset_"+countCartella+"\\asset_"+countFile+".json");
                    JSONValue.writeJSONString(dataTableFather, file);
                    file.flush();
                    file.close();
                    countFile++;
                    countFileTemp++;
                    if (countFileTemp==500 ) {

                        //zip folder
                     //   ZipUtils appZip = new ZipUtils();
                    //    appZip.generateFileList(new File("D:\\visibility_"+countCartella));
                    //    appZip.zipIt("D:\\visibility_"+countCartella+ ".zip");
                        countCartella++;
                        countFileTemp=0;
                    }

                    count=1;
                    dataList = new JSONArray();
                    dataTableFather = new LinkedHashMap();
                }
                line= br.readLine();
            }
            if (count >1){
                dataTableFather.put("scan-store-admin-asset",dataList);

                // System.out.println(JSONValue.toJSONString(dataTableFather));
                File fileFolder = new File("D:\\asset_"+ countCartella);
                if (!fileFolder.exists()) {
                    if (fileFolder.mkdir()) {
                        System.out.println("Directory is created!");
                    } else {
                        System.out.println("Failed to create directory!");
                    }
                }
                // Write to file
                FileWriter file = new FileWriter("D:\\asset_"+countCartella+"\\asset_"+countFile+".json");
                JSONValue.writeJSONString(dataTableFather, file);
                file.flush();
                file.close();
            }


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
