package com.layer9;

import org.json.simple.JSONObject;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.zip.GZIPOutputStream;


public class Main {


    public static void main(String[] args) throws FileNotFoundException {

        int countmonth=1;
        String directory="";
        String nomeFile="";
  //      while (countmonth<=12) {
            String csvFile = "C:\\untagged\\postfillevents_untagged_from_postgres\\postfillevents_untagged_2020.csv";
           /* if (countmonth<10)
                csvFile = "D:\\postfillevents_from_postgres\\postfillevents_20200"+countmonth+".csv";*/

            String line = "";
            String cvsSplitBy = ",";
            int countFile = 1;
            int countFileTemp = 0;
            int countCartella = 0;
            // write your code here
            BufferedReader br = null;
            JSONObject dataList = new JSONObject();
            JSONObject json = new JSONObject();
            // Map  dataTableFather = new LinkedHashMap();
            String result = "";
            try {
                int count = 0;
                String[] headerColumn = new String[30];//
                br = new BufferedReader(new FileReader(csvFile));
                line = br.readLine();
                while (line != null) {
                    Map dataDetails = new LinkedHashMap();
                    Map dataItem = new LinkedHashMap();
                    Map dataPutRequest = new LinkedHashMap();

                    // use comma as separator
                    line = line.replace("\"", "");
                    String[] data = line.split(cvsSplitBy);

                    if (count == 0) {
                        //header column
                        for (int i = 0; i < data.length; i++) {
                       /* if (data[i].trim().equals("id"))
                            headerColumn[i]="tagid";
                        else*/
                            headerColumn[i] = data[i].trim().toLowerCase();
                        }

                    } else {
                        for (int i = 0; i < data.length; i++) {
                            // System.out.println(headerColumn[i]);
                            if (!data[i].equals("")) {

                                dataDetails.put("\"" + headerColumn[i] + "\"", "\"" + data[i].trim() + "\"");
                            }
                        }
                        dataDetails.put("\"mType\"", "\"postfillScan\"");

                        result += dataDetails.toString() + "\n";
                        result = result.replaceAll("=",":");

                        //  dataList.put(dataDetails);
                    }
                    count++;

                    if (count == 40000) {
                        //Write JSON file

                        //   dataTableFather.put("scan-store-admin-asset",dataList);

                        // System.out.println(JSONValue.toJSONString(dataTableFather));
                        directory = "postfillevents_2020";//+countmonth;
                       /* if (countmonth<10)
                            directory = "postfillevents_20200"+countmonth;*/
                            File fileFolder = new File(directory);
                        if (!fileFolder.exists()) {
                            if (fileFolder.mkdir()) {
                                System.out.println("Directory is created!");
                            } else {
                                System.out.println("Failed to create directory!");
                            }
                        }
                        // Write to file
                        nomeFile= "postfillevent-untagged-from-postgres-" + countFile + "-2020";//-"+countmonth;
                       /* if (countmonth<10)
                            nomeFile= "postfillevent-processed-from-postgres-" + countFile + "-2020-0"+countmonth;*/
                        PrintWriter out = new PrintWriter(new FileOutputStream(directory+"\\"+nomeFile));
                        out.println(result);
                        //    file.write(result);
                        //    JSONValue.writeJSONString(json, file);
                        countFile++;
                        out.flush();
                        out.close();
                        count = 1;
                        result = "";


                    }
                    line = br.readLine();
                }
                if (count > 1) {
                    File fileFolder = new File(directory);
                    if (!fileFolder.exists()) {
                        if (fileFolder.mkdir()) {
                            System.out.println("Directory is created!");
                        } else {
                            System.out.println("Failed to create directory!");
                        }
                    }
                    // Write to file
                    PrintWriter out = new PrintWriter(new FileOutputStream(directory+"\\"+nomeFile));
                    out.println(result);
                    //    file.write(result);
                    //    JSONValue.writeJSONString(json, file);
                    countFile++;
                    out.flush();
                    out.close();

                   // gzipFile(directory+"\\"+nomeFile,
                   //         directory +"\\"+nomeFile+".gz");
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            countmonth++;
       // }

    }
    public static void gzipFile(String source_filepath, String destinaton_zip_filepath) {

        byte[] buffer = new byte[2048];

        try {

            FileOutputStream fileOutputStream =new FileOutputStream(destinaton_zip_filepath);

            GZIPOutputStream gzipOuputStream = new GZIPOutputStream(fileOutputStream);

            FileInputStream fileInput = new FileInputStream(source_filepath);

            int bytes_read;

            while ((bytes_read = fileInput.read(buffer)) > 0) {
                gzipOuputStream.write(buffer, 0, bytes_read);
            }

            fileInput.close();

            gzipOuputStream.finish();
            gzipOuputStream.close();

            System.out.println("The file was compressed successfully!");

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
