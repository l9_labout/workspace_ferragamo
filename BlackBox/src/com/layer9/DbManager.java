/*
 * DbManager.java
 *
 * Created on 6 aprile 2007, 17.00
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.layer9;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Naomi Poli
 */
public class DbManager {
    private static Connection conn;
    static String login = Parameters.getStrValue("LOGIN");
    static String port = Parameters.getStrValue("PORT");
    static String pssw = Parameters.getStrValue("PASSW");
    static String connection = Parameters.getStrValue("CONNECTION");
    static String database = Parameters.getStrValue("DATABASE");
    static String host = Parameters.getStrValue("HOST");



    public static Connection getConnection() throws Exception
    {
        if (conn==null){


            try{
                String connectionString =connection+":"+host+":"+port+":"+database;
                UpdMngLogger.info(DbManager.class.getName(),"connection string "+ connectionString);
                DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
                conn = DriverManager.getConnection(connectionString, login, pssw);
                System.out.println("connesso"); //log
            } catch (SQLException ex){
                System.out.println("\n<< SqlManager.getConnection: "+ex.getMessage()+" >>");
                System.exit(1);
            }
            return conn;
        } else{
            return conn;
        }
    }


    public static void closeConnection(){
        try {
            if (conn!=null && !conn.isClosed()){
                conn.close();
                conn=null;
                System.out.println("disconnesso");
            }
        } catch (SQLException ex){
            System.out.println("\n<< SqlManager.getConnection: "+ex.getMessage()+" >>");
            System.exit(1);
        }
    }
    
}
