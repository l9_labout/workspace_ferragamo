package com.layer9;


import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * @author Naomi Poli
 *
 */
public class App {


    public static void convertBMP(String filePath,String outputFileName) {


        try {
            BufferedImage orginalImage = ImageIO.read(new File(filePath));
            ZPLConveter zp = new ZPLConveter();
            zp.setCompressHex(false);
            String imageTemplate = zp.convertfromImg(orginalImage);
            FileOutputStream fos = new FileOutputStream(outputFileName + ".grf");
            fos.write(imageTemplate.getBytes());
            fos.close();

            System.out.println("Finished!  Check for file \"" + outputFileName + ".grf\" in executing dir");


        } catch (FileNotFoundException ex) {
            System.out.println("Error.  No file found at path: " + filePath);
            System.exit(1);
        } catch (IOException ex) {
            System.out.println("Error.  No file found at path: " + filePath);
            System.exit(1);
        } catch (Exception ex) {
        System.out.println("Error.: " + ex.getMessage());
        System.exit(1);
    }
    }

    public static void convertJapan(String filePath,String outputFileName) {


        try {
            BufferedImage orginalImage = ImageIO.read(new File(filePath));
            ZPLConveter zp = new ZPLConveter();
            zp.setCompressHex(false);
            String imageTemplate = zp.convertfromImgJapan(orginalImage);
            FileOutputStream fos = new FileOutputStream(outputFileName + ".grf");
            fos.write(imageTemplate.getBytes());
            fos.close();

            System.out.println("Finished!  Check for file \"" + outputFileName + ".grf\" in executing dir");


        } catch (FileNotFoundException ex) {
            System.out.println("Error.  No file found at path: " + filePath);
            System.exit(1);
        } catch (IOException ex) {
            System.out.println("Error.  No file found at path: " + filePath);
            System.exit(1);
        } catch (Exception ex) {
            System.out.println("Error.: " + ex.getMessage());
            System.exit(1);
        }
    }








}
