/*
 * LoadImage.java
 *
 * Created on 12 marzo 2007, 11.12
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.layer9;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import oracle.jdbc.OracleResultSet;
import oracle.sql.CLOB;

/**
 *
 * @author Naomi Poli
 */
public class LoadImage {
    private Connection conn = null;
    
    public LoadImage(Connection c) 
    {
        this.conn=c;
    }
    
   // public static void proccessFiles(String path)
    public void processFiles(String path)
        throws SQLException, IOException, ClassNotFoundException
    {
        File f = new File(path);
        File f_v = null;
        File flog = null;
        FileOutputStream fos = null;
        FileInputStream fis = null;
        String pre_v = "";
        String est_v = "";
        String immagineID = null;
        String collezione_anno = null;
        OutputStream cos = null;
        InputStream r_v = null;
        Statement stmt = conn.createStatement();
        java.util.Date curDate = Calendar.getInstance().getTime();
        DateFormat dfl = new SimpleDateFormat("dMMHmm");
        flog = new File(f, dfl.format(curDate) + ".log");
        fos = new FileOutputStream(flog);
        if(flog.canWrite())
            System.out.println("flog.canWrite() " +flog.canWrite());
            if(f.isDirectory())
            {
                System.out.println("f.isDirectory() " +f.isDirectory());
                String dirlist[] = f.list();

                for(int i = 0; i < dirlist.length; i++) {
                    try
                    {
                        try
                        {
                            System.out.println("dirlist[i] " +dirlist[i]);
                            if(dirlist[i].toUpperCase().endsWith("DEL") || dirlist[i].toLowerCase().endsWith("grf"))
                            {
                                System.out.println("dirlist[i].length() " +dirlist[i].length());
                                if(dirlist[i].length() != 17)
                                    throw new NumberFormatException("Formato nome file " + dirlist[i] + " errato\n");
                                est_v = dirlist[i].substring(14).toUpperCase();
                                pre_v = dirlist[i].substring(0, 7).toUpperCase();
                                System.out.println("est_v " +est_v);
                                System.out.println("pre_v " +pre_v);
								collezione_anno = dirlist[i].substring(7, 13).toUpperCase();
                                System.out.println("collezione_anno " +collezione_anno);
                                immagineID = pre_v;
                                if(est_v.compareTo("DEL") == 0)
                                {
                                    f_v = new File(f, dirlist[i]);
                                    stmt.executeUpdate("DELETE IMMAGINE WHERE IMMAGINE_ID = '" + immagineID + "' AND COLLEZIONE = '" + collezione_anno + "'");
                                    conn.commit();
                                    f_v.delete();
                                    fos.write(("delete file : " + dirlist[i] + "\n").getBytes());
                                }
                                if(est_v.compareTo("GRF") == 0)
                                {
                                    stmt.executeUpdate("DELETE IMMAGINE WHERE IMMAGINE_ID = '" + immagineID + "' AND COLLEZIONE = '" + collezione_anno + "'");
                                    stmt.executeUpdate("INSERT INTO IMMAGINE VALUES ('" + immagineID + "', EMPTY_CLOB(), '" + collezione_anno + "')");
                                    ResultSet rs = stmt.executeQuery("SELECT IMMAGINE FROM IMMAGINE WHERE IMMAGINE_ID='" + immagineID + "' AND COLLEZIONE = '" + collezione_anno + "' FOR UPDATE");
                                    rs.next();
                                    CLOB clob_v = ((OracleResultSet)rs).getCLOB("immagine");
                                    cos = clob_v.getAsciiOutputStream();
                                    f_v = new File(f, dirlist[i]);
                                    r_v = new FileInputStream(f_v);
                                    int intero = r_v.read();
                                    int c = 0;
                                    for(; intero != -1; intero = r_v.read())
                                        c++;
                                    int arrayInt[] = new int[c];
                                    byte arrayByte[] = new byte[c];
                                    r_v.close();
                                    r_v = new FileInputStream(f_v);
                                    c = -1;
                                    for(intero = r_v.read(); intero != -1; intero = r_v.read())
                                    {
                                        c++;
                                        arrayInt[c] = intero;
                                    }
                                    for(int k = 0; k <= c; k++)
                                        arrayByte[k] = (byte)arrayInt[k];
                                    cos.write(arrayByte);
                                    r_v.close();
                                    cos.close();
                                    //conn.commit();
                                    f_v.renameTo(new File(f, pre_v + collezione_anno + ".ok"));
                                    fos.write(("insert file : " + dirlist[i] + "\n").getBytes());
                                    //Inizio modifica

                                        fos.write(("id immagine: " + immagineID.substring(1) + "\n").getBytes());
                                        fos.write(("collezione: " + collezione_anno + "\n").getBytes());
                                        stmt.executeUpdate("DELETE from IMG_IMPORT WHERE CODICE_MODELLO = '" + immagineID.substring(1) + "' AND COLLEZIONE = '" + collezione_anno + "'");
                                        conn.commit();
                                    //Fine modifica
                                }
                            }
                            continue;
                        }
                        catch(NumberFormatException e)
                        {
                            fos.write((e.getMessage() + "Formato nome file : " + dirlist[i] + "  errato, aggiornamento non effettuato\n").getBytes());
                            continue;
                        }
                        catch(SQLException e)
                        {
                            conn.rollback();
                            fos.write(("Nome File : " + dirlist[i] + "  eccezione oracle messaggio : " + e.getErrorCode() + " " + e.getMessage() + "\n").getBytes());
                            continue;
                        }
                        catch(Exception e)
                        {
                            conn.rollback();
                            fos.write(("Nome File : " + dirlist[i] + "  eccezione messaggio : " + e.getMessage() + "\n").getBytes());
                        }
                        continue;
                    }
                    finally
                    {
                        if(r_v != null)
                            r_v.close();
                        if(cos != null)
                            cos.close();
                    }
                }
                PrintStream pout = new PrintStream(fos);
                pout.close();
            } else
            {
                throw new IOException("Protezione scrittura su directory : " + path);
            }
    }//chiusa del metodo proccessFiles
}//chiusura della classe