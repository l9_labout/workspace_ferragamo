package com.layer9;



public abstract class BaseLogger
{
	static public final int INFO_LEV = 0;
	static public final int DEBUG_LEV = 1;
	static public final int WARN_LEV = 2;
	static public final int ERROR_LEV = 3;
	static public final int FATAL_LEV = 4;

	static public final String levelStr[] = {"Info", "Debug", "Warning", "Error", "Fatal"};

	abstract public void fatal(Object object, Object message);

	abstract public void info(Object object, Object message);

	abstract public void debug(Object object, Object message);

	abstract public void warn(Object object, Object message);

	abstract public void error(Object object, Object message);
}
