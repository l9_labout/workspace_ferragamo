/*
 * GmcUtils.java
 *
 * Created on 8 maggio 2007, 12.05
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.layer9;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
/**
 * 
 * @author Poli Naomi
 */
public class GmcUtils {
    
    /**
     * Creates a new instance of GmcUtils
     */
    public GmcUtils() {}
    //metodo
    public static void logga(String txt,Connection conn){
        try {
            Statement stmtLogga = conn.createStatement();
            stmtLogga.executeUpdate("INSERT INTO logga VALUES ('" + txt + "',sysdate)");
            stmtLogga.close();
            conn.commit();
        }catch(Exception errrr) {
            errrr.printStackTrace();
        }
    }
    
    //metodo che passata una stringa  e restituisce il valore del parametro
    public static String getGmcValore(String p_param,Connection conn) throws SQLException {
        Statement stmt = conn.createStatement();
        String result="";
        ResultSet rs = stmt.executeQuery("select valore from configurazione_sistema where parametro = '" + p_param + "'");
        if(rs.next())
            result= rs.getString("valore");
        else
            throw new SQLException("Parametro '" + p_param + "' non trovato");

        rs.close();
        stmt.close();
        return result;
    }
}
