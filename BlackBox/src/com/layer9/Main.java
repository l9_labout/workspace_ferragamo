package com.layer9;

import java.io.File;
import java.util.Properties;

public class Main {
    public Main(String[] args) {
        //cancella i log
        Properties prop = Parameters.getConfigProperties("GlobParameters.properties");
        cancellaLog("log/",prop.getProperty("DAYS_LOG"));
        if(args.length>0)
        {

            if("J".equalsIgnoreCase(args[0]))
            //Conversione immagini Giappone
            {
                ConvertImgJapan cv = new ConvertImgJapan();
                cv.convert();
            }
            else if("S".equalsIgnoreCase(args[0]))
            //Conversione immagini Standard
            {
                ConvertImgStandard cv = new ConvertImgStandard();
                cv.convert();
            }
            else
            {
                System.out.println("Parametro errato: "+args[0]);
                UpdMngLogger.error(this,"Parametro errato: "+args[0]);
            }
        }
        else
        {
            //Conversione immagini Standard
            ConvertImgStandard cv = new ConvertImgStandard();
            cv.convert();
            //Conversione immagini Giappone
           ConvertImgJapan cvJ = new ConvertImgJapan();
           cvJ.convert();
        }
    }
    public static void main(String[] args) {
        logEnvVariables();
        new Main(args);
    }

    private static void logEnvVariables()
    {
        java.util.Properties prop = System.getProperties();
        java.util.Enumeration e = prop.propertyNames();
        UpdMngLogger.info(Main.class.getName(), "*** System Environment As Seen By Java");
        UpdMngLogger.debug(Main.class.getName(), "*** Format: PROPERTY = VALUE***");
        while(e.hasMoreElements())
        {
            String key = (String) e.nextElement();
            UpdMngLogger.info(Main.class.getName(), key + " = " + System.getProperty(key));
        }
    }
    private static void cancellaLog(String dirname, String days) {
        try {
            UpdMngLogger.info(Main.class.getName(),"Cancellazione file log piu' vecchi di "+days+" gg...");
            Long range = Long.valueOf(days)*24*60*60*1000;
            long today = System.currentTimeMillis();
            File dirlog = new File(dirname);

            if ((!dirlog.exists()) || (!dirlog.isDirectory())) {
                return;
            }
            File[] lista = dirlog.listFiles();
            for (int i = 0; i < lista.length; i++) {
                long mod = lista[i].lastModified();
                if (today - mod > range)
                    if (lista[i].getName().contains("log")){
                        lista[i].delete();
                    }

            }
        } catch (Exception e) {
            UpdMngLogger.info(Main.class.getName(),"Cancella_Log " + e.getMessage());
        }
    }
}
