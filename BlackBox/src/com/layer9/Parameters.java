package com.layer9;

/**
 * <p>Title: BlackBox</p>
 * <p>Description: Elaborazione img standard e giapponesi</p>
 * <p>Copyright: (c) 2020 Layer9</p>
 * <p>Company: Layer9</p>
 *
 * @author Naomi Poli
 * @version 1.0
 */



import java.io.FileInputStream;
import java.util.Properties;

public class Parameters
{
	private static Properties prop = null;
	private static final String configFile = "dbParameters.properties";
	final static String subdir_config = "config";




	public static Properties getConfigProperties(String configFile)
	{
		Properties prop = new Properties();
		try
		{

			FileInputStream is = new FileInputStream("./"+subdir_config+"/"+configFile+"");
			prop.load(is);
		} catch(Exception e)
		{
			/*ServerLog.error(ServerParameters.class.getName(),
					"Reading CommonConfig properties :" + configFile + " - " + e.toString());*/
		}
		return prop;
	}

	static public Properties getProperties()
	{
		if(prop == null)
		{
			prop = getConfigProperties(configFile);
		}
		return prop;
	}

	static public String getStrValue(String name)
	{
		try
		{
			String str = Parameters.getProperties().getProperty(name);
			return str;
		} catch(Exception e)
		{
			//ServerLog.error(ServerParameters.class.getName(), "Not found key " + name + " on " + configFile);
			return "";
		}
	}

	static final public String LISPORT = "port";
	static final public String DBDRIVER = "DbDriver";
	static final public String USR_DB_NAME = "userDbName";
	static final public String USR_DB_PSWD = "userDbPass";
	static final public String MAX_CON_DB = "maxCon";
	static final public String INIT_CON_DB = "initCon";
	static final public String WAITIFBUSY_DB = "waitIfBusy";



}
