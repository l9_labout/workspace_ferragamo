package com.layer9;


import java.io.*;
import java.util.*;
import javax.swing.*;

public class UpdMngLogger
{
	static final private String UPDMNG_LOG_LEVEL = "UpdMngLogLevel";

	static class Logger extends BaseLogger
	{

		static String LogFileName = "blackbox.log"; //nome di default del logger

		int level = ERROR_LEV;
		String levelStr[] = {"Info", "Debug", "Warning", "Error", "Fatal"};
		BufferedWriter logwriter = null;

		public Logger(int level)
		{
			try
			{
				LogFileName = setLoggerFile();
				logwriter = new BufferedWriter(new FileWriter(new File("log", LogFileName), true));

				this.level = (level >= 0 && level <= FATAL_LEV) ? level : WARN_LEV;
				internalLog(0, "UpdMngLogger", "Start Log - Level:" + levelStr[level]);
			} catch(Exception ex)
			{
				System.err.println(ex.getMessage());
			}
		}

		public void fatal(Object object, Object message)
		{
			internalLog(FATAL_LEV, object, message);
		}

		public void info(Object object, Object message)
		{
			if(level <= INFO_LEV)
				internalLog(INFO_LEV, object, message);
		}

		public void debug(Object object, Object message)
		{
			if(level <= DEBUG_LEV)
				internalLog(DEBUG_LEV, object, message);
		}

		public void warn(Object object, Object message)
		{
			if(level <= WARN_LEV)
				internalLog(WARN_LEV, object, message);
		}

		public void error(Object object, Object message)
		{
			internalLog(ERROR_LEV, object, message);
		}

		private void internalLog(int type, Object classname, Object message)
		{
			String str = null;
			try
			{
				String clazz = (classname instanceof String) ? (String) classname :
						classname.getClass().getName();
				str = levelStr[type] + " : [" + message.toString() + "] in class  [" + clazz + "]";
				logwriter.write("[" + Calendar.getInstance().getTime().toString() + "] " + str + "\n");
				logwriter.flush();
				System.out.println(str);
			} catch(Exception ex)
			{
				System.out.println(str);
				System.err.println(ex.getMessage());
			}
		}
	}

	private static BaseLogger logger = null;

	private UpdMngLogger()
	{
	}

	static public int getUpdMngLogLevel()
	{
		try
		{
			String configFile = "GlobParameters.properties";
			Properties prop = new Properties();
			File file = new File(configFile);
			if(!file.isAbsolute())
			{
				file = new File("config", configFile);
			}
			FileInputStream is = new FileInputStream(file);
			prop.load(is);
			String str = prop.getProperty(UPDMNG_LOG_LEVEL);
			int i = Integer.parseInt(str);
			return i;
		} catch(Exception e)
		{
		}
		return -1;
	}

	private static BaseLogger getLogger()
	{
		if(logger == null)
		{
			int lev = getUpdMngLogLevel();
			if(lev == -1) lev = Logger.WARN_LEV;
			logger = new Logger(lev);
		}
		return logger;
	}

	static public void fatal(Object object, Object message)
	{
		getLogger().fatal(object, message);
	}

	static public void info(Object object, Object message)
	{
		getLogger().info(object, message);
	}

	static public void debug(Object object, Object message)
	{
		getLogger().debug(object, message);
	}

	static public void warn(Object object, Object message)
	{
		getLogger().warn(object, message);
	}

	static public void error(Object object, Object message)
	{
		getLogger().error(object, message);
	}

	public static void JlogErr(Object object, String message, java.awt.Component component)
	{
		getLogger().error(object, message);
		JOptionPane.showMessageDialog(component, message, "Attenzione", JOptionPane.ERROR_MESSAGE);
	}

	public static void JlogWar(Object object, String message, java.awt.Component component)
	{
		getLogger().warn(object, message);
		JOptionPane.showMessageDialog(component, message, "Attenzione", JOptionPane.WARNING_MESSAGE);
	}

	public static void JlogInfo(Object object, String message, java.awt.Component component)
	{
		getLogger().warn(object, message);
		JOptionPane.showMessageDialog(component, message, "Attenzione", JOptionPane.INFORMATION_MESSAGE);
	}

	public static void JlogErr(Object object, String message)
	{
		JlogErr(object, message, null);
	}

	public static void JlogWar(Object object, String message)
	{
		JlogWar(object, message, null);
	}

	public static void JlogInfo(Object object, String message)
	{
		JlogInfo(object, message, null);
	}

	static public void setLogger(BaseLogger logger)
	{
		UpdMngLogger.logger = logger;
	}

	//        private static JOptionPane myOpt = new JOptionPane();
	private static JDialog myDia;

	public static void removeStaticInfo()
	{
		if(myDia != null) myDia.dispose();
	}

	public static void JlogStaticInfo(Object object, String message, int tempo)
	{
		getLogger().warn(object, message);
		if(myDia != null) myDia.dispose();
		JOptionPane myOpt = new JOptionPane(message, JOptionPane.INFORMATION_MESSAGE);
		myOpt.setOptions(new Object[]{});
		myDia = myOpt.createDialog(null, "Attenzione");
		myDia.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		ChiudiFinestra closer = new ChiudiFinestra(myDia, tempo);
		closer.start();
		myDia.setVisible(true);
		myDia.setEnabled(false);
	}

	public static void JlogStaticInfoBenc(Object object, String message)
	{
		getLogger().warn(object, message);
		if(myDia != null) myDia.dispose();
		JOptionPane myOpt = new JOptionPane(message, JOptionPane.INFORMATION_MESSAGE);
		myOpt.setOptions(new Object[]{});
		myDia = myOpt.createDialog(null, "Attenzione");
		myDia.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		ChiudiFinestra closer = new ChiudiFinestra(myDia, 5);
		closer.start();
		myDia.setVisible(true);
		myDia.setEnabled(false);
	}





	static class ChiudiFinestra extends Thread
	{
		private JDialog myDia;
		private int tempo;

		public ChiudiFinestra(JDialog myDia, int tempo)
		{
			this.myDia = myDia;
			this.tempo = tempo * 1000;
		}

		public void run()
		{
			try
			{
				this.sleep(tempo);
			} catch(InterruptedException intEx)
			{
			}
			myDia.dispose();
		}
	}


	public static String setLoggerFile()
	{
		//data di sistema
		Long lon = new Long(System.currentTimeMillis());
		java.sql.Date data = new java.sql.Date(lon.longValue());
		java.text.SimpleDateFormat extformat = new java.text.SimpleDateFormat(
				"yyyy-MM-dd");
		String datasist = extformat.format(data);
		String logFileName = "blackbox" + datasist + ".log"; //log file
		return logFileName;
	}


}
