package com.layer9;



import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.*;
import java.awt.Graphics2D;
import java.io.*;
import java.sql.Connection;
import java.util.Properties;



import javax.imageio.ImageIO;
import javax.swing.*;


public class ImageProcessing {

    private static Properties prop= null;


    public static void getImageProcessedStandard(String inputImagePath,String nome){
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        Mat source = Imgcodecs.imread(inputImagePath);

        //Creating destination matrix
        Mat destination = new Mat(source.rows(), source.cols(), source.type());
        int erosion_size = 1;

        Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(2*erosion_size + 1, 2*erosion_size+1));
        Imgproc.erode(source, destination, element);
        Imgcodecs.imwrite(nome + ".bmp", destination);


    }

    public static void getImageProcessedJapan(String inputImagePath,String nome){
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        Mat source = Imgcodecs.imread(inputImagePath);

        //Creating destination matrix
        Mat destination = new Mat(source.rows(), source.cols(), source.type());
        double erosion_size = 0.5;

        Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(2*erosion_size + 1, 2*erosion_size+1));
        Imgproc.erode(source, destination, element);
        Imgcodecs.imwrite(nome + ".bmp", destination);


    }





    /*public static void convertBmpToTif (BufferedImage bmp, String name){
        TIFFImageWriterSpi spi = new TIFFImageWriterSpi();
        ImageWriter writer = null;
        try {
            writer = spi.createWriterInstance();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ImageWriteParam param = writer.getDefaultWriteParam();
        param.setCompressionMode(ImageWriteParam.MODE_DISABLED);

        ImageOutputStream ios = null;
        try {
            ios = ImageIO.createImageOutputStream(new File(name+".tif"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        writer.setOutput(ios);
        try {
            writer.write(null, new IIOImage(bmp, null, null), param);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/
    public static void deleteImgTifGrf(String imgconv_path, Connection conn){
        File out_path = new File(imgconv_path);
        UpdMngLogger.info(ImageProcessing.class.getName(),"Inizio eliminazione file GRF e BMP...\n".getBytes());
        if(out_path.isDirectory())
        {
            File f_to_del[] = out_path.listFiles();
            for(int i = 0; i < f_to_del.length - 1; i++)
                if(f_to_del[i].isFile() &&(f_to_del[i].toString().toLowerCase().endsWith("grf")
                        ||  f_to_del[i].toString().toLowerCase().endsWith("bmp")
                        ||  f_to_del[i].toString().toLowerCase().endsWith("ok")
                ))
                {
                    UpdMngLogger.info(ImageProcessing.class.getName(),(" Eliminazione " + f_to_del[i].getAbsoluteFile() + "\n").getBytes());
                    f_to_del[i].delete();
                }
        }
        UpdMngLogger.info(ImageProcessing.class.getName(),"Fine eliminazione file BMP e GRF\n".getBytes());
        GmcUtils.logga("Fine eliminazione file BMP e GRF",conn);
    }


    /*
Change dimension of Image
*/
    public static void resizeImage(String inputImagePath, String outputImagePath, int scaledWidth, int scaledHeight, boolean preserveRatio,boolean checkCalzature) {

        // creates output image
        File pathToFile = new File(inputImagePath);
        Image image = null;
        try {
            image = ImageIO.read(pathToFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (preserveRatio) {
            ImageObserver w = new ImageObserver() {
                @Override
                public boolean imageUpdate(Image img, int infoflags, int x, int y, int width, int height) {
                    return false;
                }
            };
            //altezza 350 stivale
            double imageHeight = (double) image.getHeight(w);
            double imageWidth = image.getWidth(w);

            if (imageHeight/scaledHeight > imageWidth/scaledWidth) {
                scaledWidth = (int) (scaledHeight * imageWidth / imageHeight);
            } else {
                scaledHeight = (int) (scaledWidth * imageHeight / imageWidth);
            }
        }
        BufferedImage sfondo = null;
        try {

           sfondo = ImageIO.read(new File("./config/", "sfondo.bmp"));
            //sfondo = ImageIO.read(new File("C:\\workspace_ferragamo\\BlackBox", "sfondo.bmp"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedImage inputBufImage = toBufferedImage(image);
        // creates output image
        BufferedImage outputBufImage =null;
        if (checkCalzature) {
            outputBufImage = new BufferedImage(260, 170, inputBufImage.getType());
        }else
        {
            outputBufImage = new BufferedImage(scaledWidth, scaledHeight, inputBufImage.getType());
        }

        // scales the input image to the output image
        int w= (260-scaledWidth)/2;

        Graphics g2d = outputBufImage.createGraphics();
        if (checkCalzature) {
            g2d.drawImage(sfondo, 0, 0, null);
            g2d.drawImage(inputBufImage, w, 0, scaledWidth, scaledHeight, null);
        }else
        {
            g2d.drawImage(inputBufImage, 0, 0, scaledWidth, scaledHeight, null);
        }
        g2d.dispose();
        // extracts extension of output file
        String formatName = outputImagePath.substring(outputImagePath
                .lastIndexOf(".") + 1);

        // writes to output file
        try {
            ImageIO.write(outputBufImage, formatName, new File(outputImagePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        File inputFile1 = new File(outputImagePath);
        try {
            BufferedImage inputImage1 = ImageIO.read(inputFile1);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    private static int colorToRGB(int alpha, int red, int green, int blue) {
        int newPixel = 0;
        newPixel += alpha;
        newPixel = newPixel << 8;
        newPixel += red;
        newPixel = newPixel << 8;
        newPixel += green;
        newPixel = newPixel << 8;
        newPixel += blue;

        return newPixel;
    }
    /**
     * Resizes an image to a absolute width and height (the image may not be
     * proportional)
     * @param inputImagePath Path of the original image
     * @param outputImagePath Path to save the resized image
     * @param scaledWidth absolute width in pixels
     * @param scaledHeight absolute height in pixels
     * @throws IOException
     */
    public static void resize(String inputImagePath,
                              String outputImagePath, int scaledWidth, int scaledHeight)
            throws IOException {
        // reads input image
        File inputFile = new File(inputImagePath);
        int w = scaledWidth;
        int h = scaledHeight;
        int pixels[] = new int[w * h];
        int i = 0;
        for(int y = 0; y < h; y++) {
            for(int x = 0; x < w; x++) {
                int r = 0 & 0xff;
                int g = 255 & 0xff;
                int b = 0 & 0xff;
                pixels[i++] = (255 << 24) | (r << 16) | (g << 8) | b;
            }
        }
        BufferedImage inputImage = ImageIO.read(inputFile);
        inputImage.setRGB(scaledWidth, scaledHeight,0,0, pixels,1,0);
        // creates output image
        BufferedImage outputImage = new BufferedImage(scaledWidth,
                scaledHeight, inputImage.getType());


        // scales the input image to the output image
        Graphics2D g2d = outputImage.createGraphics();
        g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
        g2d.dispose();

        // extracts extension of output file
        String formatName = outputImagePath.substring(outputImagePath
                .lastIndexOf(".") + 1);

        // writes to output file
        ImageIO.write(outputImage, formatName, new File(outputImagePath));
        File inputFile1 = new File(outputImagePath);
        BufferedImage inputImage1 = ImageIO.read(inputFile1);


        // creates output image
        BufferedImage outputImage2 = new BufferedImage(scaledWidth,
                scaledHeight, inputImage1.getType());
        Image image =new ImageIcon(outputImage2).getImage();


//Il risultato lo inseriamo in un Image
      /* BufferedImage outputImage1 = toBufferedImage(outputImage3);
        // scales the input image to the output image
        Graphics2D g2d1 = outputImage1.createGraphics();
        g2d1.drawImage(outputImage1, 0, 0, scaledWidth, scaledHeight, null);
        g2d1.dispose();
        String imgOutFile = "C:/ferragamo/blackbox/Img/A0000000_final.BMP";
        // extracts extension of output file
        String formatName1 = outputImagePath.substring(imgOutFile
                .lastIndexOf(".") + 1);

        // writes to output file
        ImageIO.write(outputImage1, formatName, new File(imgOutFile));*/
    }

    private static BufferedImage createBorderedImage(final Image image, final Color color) {
        int w = image.getWidth(null);
        int h = image.getHeight(null);
        BufferedImage bufferedImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = bufferedImage.createGraphics();
        g2d.setStroke(new BasicStroke(3.0f));
        g2d.setColor(color);
        g2d.drawRect(0, 0, w - 1, h - 1);
        g2d.drawImage(image, 0, 0, null);
        g2d.dispose();

        return bufferedImage;
    }
    private static BufferedImage createColouredImage(Image bwImage, final Color renderCol) {
        // create coloured image
        RGBImageFilter rgbFilter = new RGBImageFilter() {

            @Override
            public int filterRGB(int x, int y, int rgb) {
                Color current = new Color(rgb);
                int red = current.getRed();
                if (current.getAlpha() == 0 || red == 0) {
                    return 0;
                }
                double factor = red / 255.0;

                int newRed = to0To255Int(renderCol.getRed() * factor);
                int newBlue = to0To255Int(renderCol.getBlue() * factor);
                int newGreen = to0To255Int(renderCol.getGreen() * factor);
                Color newCol = new Color(newRed, newGreen, newBlue, renderCol.getAlpha());
                return newCol.getRGB();
            }
        };

        // transform to colour and save in colour
        ImageProducer ip = new FilteredImageSource(bwImage.getSource(), rgbFilter);
        Image coloured = Toolkit.getDefaultToolkit().createImage(ip);
        BufferedImage colourImage = toBufferedImage(coloured);
        return colourImage;
    }


    private static int to0To255Int(double v) {
        return ensureRange((int) Math.round(v));
    }
    private static int ensureRange(int val) {
        if (val < 0) {
            return 0;
        }
        if (val > 255) {
            return 255;
        }
        return val;
    }
    public static BufferedImage toBufferedImage(Image img)
    {
        if (img instanceof BufferedImage)
        {
            return (BufferedImage) img;
        }

        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();

        // Return the buffered image
        return bimage;
    }
    private static Image changeBrightness(Image img, final boolean brighten, final int percent) {
        ImageFilter filter = new RGBImageFilter() {
            public int filterRGB(int x, int y, int rgb) {
                return (rgb & 0xff000000) | (filter(rgb >> 16) << 16) | (filter(rgb >> 8) << 8) | (filter(rgb));
            }

            /**
             * Brighens or darkens a single r/g/b value.
             */
            private int filter(int color) {
                color = color & 0xff;
                if (brighten) {
                    color = (255 - ((255 - color) * (100 - percent) / 100));
                } else {
                    color = (color * (100 - percent) / 100);
                }
                if (color < 0)
                    color = 0;
                if (color > 255)
                    color = 255;
                return color;
            }
        };
        ImageProducer prod = new FilteredImageSource(img.getSource(), filter);
        return Toolkit.getDefaultToolkit().createImage(prod);
    }

    public static void grayscale (String pathImage){
        BufferedImage img = null;
        File f = null;

        // read image
        try {
            f = new File(
                    pathImage);
            img = ImageIO.read(f);
        }
        catch (IOException e) {
            System.out.println(e);
        }

        // get image's width and height
        int width = img.getWidth();
        int height = img.getHeight();

        // convert to grayscale
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {

                // Here (x,y)denotes the coordinate of image
                // for modifying the pixel value.
                int p = img.getRGB(x, y);

                int a = (p >> 24) & 0xff;
                int r = (p >> 16) & 0xff;
                int g = (p >> 8) & 0xff;
                int b = p & 0xff;

                // calculate average
                int avg = (r + g + b) ;/// 1;

                // replace RGB value with avg
                p = (a << 24) | (avg << 16) | (avg << 8)
                        | avg;

                img.setRGB(x, y, p);
            }
        }

        // write image
        try {
            f = new File(
                    pathImage);
            ImageIO.write(img, "bmp", f);
        }
        catch (IOException e) {
            System.out.println(e);
        }
    }

    /**
     * Resizes an image by a percentage of original size (proportional).
     * @param inputImagePath Path of the original image
     * @param outputImagePath Path to save the resized image
     * @param percent a double number specifies percentage of the output image
     * over the input image.
     * @throws IOException
     */
    public static void resize(String inputImagePath,
                              String outputImagePath, double percent) throws IOException {
        File inputFile = new File(inputImagePath);
        BufferedImage inputImage = ImageIO.read(inputFile);
        int scaledWidth = (int) (inputImage.getWidth() * percent);
        int scaledHeight = (int) (inputImage.getHeight() * percent);
        resize(inputImagePath, outputImagePath, scaledWidth, scaledHeight);
    }

    /**
     * scale image
     *
     * @param sbi image to scale
     * @param imageType type of image
     * @param dWidth width of destination image
     * @param dHeight height of destination image
     * @param fWidth x-factor for transformation / scaling
     * @param fHeight y-factor for transformation / scaling
     * @return scaled image
     */
    public static BufferedImage scale(BufferedImage sbi, int imageType, int dWidth, int dHeight, double fWidth, double fHeight) {
        BufferedImage dbi = null;
        if(sbi != null) {
            dbi = new BufferedImage(dWidth, dHeight, imageType);
            Graphics2D g = dbi.createGraphics();
            AffineTransform at = AffineTransform.getScaleInstance(fWidth, fHeight);
            g.drawRenderedImage(sbi, at);
        }
        return dbi;
    }

    public static BufferedImage scale(BufferedImage imageToScale, int dWidth, int dHeight) {
        BufferedImage scaledImage = null;
        if (imageToScale != null) {
            scaledImage = new BufferedImage(dWidth, dHeight, imageToScale.getType());
            Graphics2D graphics2D = scaledImage.createGraphics();
            graphics2D.drawImage(imageToScale, 0, 0, dWidth, dHeight, null);
            graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            graphics2D.dispose();
        }
        return scaledImage;
    }

    public static BufferedImage rotateClockwise90(BufferedImage src) {
        int width = src.getWidth();
        int height = src.getHeight();

        BufferedImage dest = new BufferedImage(height, width, src.getType());

        Graphics2D graphics2D = dest.createGraphics();
        graphics2D.translate((height - width) / 2, (height - width) / 2);
        graphics2D.rotate(Math.PI / 2, height / 2, width / 2);
        graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.drawRenderedImage(src, null);

        return dest;
    }

}
