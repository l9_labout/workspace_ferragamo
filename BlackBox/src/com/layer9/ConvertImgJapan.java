
package com.layer9;

import oracle.jdbc.OracleResultSet;
import oracle.sql.CLOB;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

public class ConvertImgJapan {
    private Connection conn;
    private final static String IMPORT_PATH ="IMPORT_PATH_IMG";
    private final static String CONV_PATH ="IMG_CONV_PATH";
    private final static String IMG_PATH ="IMG_J_PATH";
    private Properties prop=null;


    public void convert()
    {
        try
        {
            conn= DbManager.getConnection();
            UpdMngLogger.info(this,"Connessione effettuata " + conn);
            prop = Parameters.getConfigProperties("GlobParameters.properties");
            Statement stmt = conn.createStatement();
            String img_path = GmcUtils.getGmcValore(IMPORT_PATH,this.conn); //GMC_IMPORT_PATH
            String imgJ=GmcUtils.getGmcValore(IMG_PATH,this.conn); //\\WEBITFI05SFG\Tempo$
            String imgconv_path = GmcUtils.getGmcValore(CONV_PATH,this.conn);//c:/images

            ImageProcessing.deleteImgTifGrf(imgconv_path,conn);
            ResultSet rs = stmt.executeQuery(prop.getProperty("BASE_SELECT"));
            do//ciclo per tutti i record presenti nel db
            {
                if(!rs.next())//nel caso non ci siano piu' record
                    break;
                GmcUtils.logga(rs.getString("SKU"),this.conn);
                UpdMngLogger.info(this,"SKU " +rs.getString("SKU"));

                //categorie che gestiscono le cinture 23 e 67.
                /*nome immagine in uscita: I + codice modello + collezione + anno */

                String in_filename = imgJ + "\\" + rs.getString("SKU")+"ETICART2J.gif"; //\\WEBITFI05SFG\Tempo$
                String out_filename = img_path + "\\" + rs.getString("SKU")+"ETICART2J.gif"; //C:\images_j C:\images_j\0432809ETICART2J
                UpdMngLogger.info(this,"in_fileName " + in_filename);
                UpdMngLogger.info(this,"out_fileName " + out_filename);

                String name = prop.getProperty("NOME_GRF_J");
                if(name ==null)
                    name="Temp";
                File inPathFileName = new File(in_filename);
                if (inPathFileName.exists()) {
                    ImageProcessing.resizeImage(in_filename, out_filename, 261,300,true,false);
                    BufferedImage imgrotate= ImageProcessing.rotateClockwise90(ImageIO.read(new File(out_filename)));
                    File outputfile = new File(out_filename);
                    ImageIO.write(imgrotate, "gif", outputfile);
                    String imgConvert = outputfile.getPath().substring(0,outputfile.getPath().length() -4);

                    ImageConverter.convertFormat(outputfile.getPath(),imgConvert +".bmp","bmp");
                    ImageProcessing.getImageProcessedJapan(imgConvert +".bmp",imgConvert);
                    App.convertJapan(imgConvert +".bmp", imgconv_path + "\\" + name);
                    File sorg_grf = new File(imgconv_path + "\\" + name + ".grf");
                    if (sorg_grf.exists()) {
                        UpdMngLogger.info(this,"file grf trovato " );
                        String nome = rs.getString("SKU") + "J";
                        sorg_grf.renameTo(new File(imgconv_path + "\\" + nome + ".grf"));
                        scrivi(imgconv_path + "\\" + nome + ".grf", rs.getString("SKU"));
                    } else //file non trovato
                        UpdMngLogger.info(this, "File grf non trovato ");
                }else{
                    UpdMngLogger.info(this,"nessuna immagine trovato " + in_filename);
                }

            } while(true);//ciclo per tutti i record presenti nel db
        }
        catch(SQLException e)
        {
            UpdMngLogger.info(this,("Errore sql : " + e.getMessage() + "\n").getBytes());
        }
        catch(Exception e)
        {
            UpdMngLogger.info(this,("Eccezione generica : " + e.getMessage() + "\n").getBytes());
            GmcUtils.logga("Eccezione generica : " + e.getMessage(),this.conn);

        }
        finally
        {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }

    }
    private void scrivi(String filename,String sku) throws java.io.IOException, java.sql.SQLException
    {
        File f_v = null;
        File flog = null;
        FileOutputStream fos = null;
        FileInputStream fis = null;
        OutputStream cos = null;
        InputStream r_v = null;
        java.util.Date curDate = Calendar.getInstance().getTime();
        DateFormat dfl = new SimpleDateFormat("dMMyy_Hmm");
        flog = new File(dfl.format(curDate) + ".log");

        try
        {
            UpdMngLogger.info(this,"SKU  " + sku);
            UpdMngLogger.info(this,"filename  " + filename);
            fos = new FileOutputStream(flog);
            Statement stmt = conn.createStatement();
            try
            {
                stmt.executeUpdate("DELETE IMMAGINE_J WHERE SKU = '" + sku+"'");
            }
            catch(Exception t)
            {
            }
            stmt.executeUpdate("INSERT INTO IMMAGINE_J VALUES ('" + sku +
                    "', EMPTY_CLOB())");
            ResultSet rs = stmt.executeQuery(
                    "SELECT IMMAGINE FROM IMMAGINE_J WHERE SKU='" + sku + "' FOR UPDATE");
            rs.next();
            CLOB clob_v = ( (OracleResultSet) rs).getCLOB("IMMAGINE");
            cos = clob_v.getAsciiOutputStream();
            f_v = new File(filename);
            r_v = new FileInputStream(f_v);
            int intero = r_v.read();
            int c = 0;
            for (; intero != -1; intero = r_v.read())
                c++;
            int arrayInt[] = new int[c];
            byte arrayByte[] = new byte[c];
            r_v.close();
            r_v = new FileInputStream(f_v);
            c = -1;
            for (intero = r_v.read(); intero != -1; intero = r_v.read()) {
                c++;
                arrayInt[c] = intero;
            }
            for (int k = 0; k <= c; k++)
                arrayByte[k] = (byte) arrayInt[k];
            cos.write(arrayByte);
            r_v.close();
            cos.close();
            //conn.commit();
            //f_v.renameTo(new File(f, pre_v + collezione_anno + ".ok"));
            fos.write( ("insert file : " + filename+ "\n").getBytes());
            //Inizio modifica
            /* Lorenzo Nencini, 14/03/2007: cancellazione record IMG_IMPORT basato su id immagine(codice modello) + collezione */
            //fos.write( ("sku: " + sku + "\n").getBytes());
            conn.commit();
            //Fine modifica
        }
        catch (NumberFormatException e)
        {
            fos.write((e.getMessage() + "Formato nome file : " + filename + "  errato, aggiornamento non effettuato\n").getBytes());
        }
        catch(SQLException e)
        {
            conn.rollback();
            fos.write(("Nome File : " + filename + "  eccezione oracle messaggio : " + e.getErrorCode() + " " + e.getMessage() + "\n").getBytes());
        }
        catch(Exception e)
        {
            conn.rollback();
            fos.write(("Nome File : " + filename + "  eccezione messaggio : " + e.getMessage() + "\n").getBytes());
        }
        finally
        {
            if(r_v != null)
                r_v.close();
            if(cos != null)
                cos.close();
        }
    }
}

